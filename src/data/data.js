import UserImage from "../assets/user.png";
import BlogImg from "../assets/blogimg.png";
import ArrowImg from "../assets/arrowimg.svg";
export const SideCards = [
  {
    userimage: UserImage,
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },

  {
    userimage: UserImage,
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },
  {
    userimage: UserImage,
    section2text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section2text2: "   3 gün önce",
  },
];

export const SideCards2 = {
  bannertext:
    "  Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",

  buttontext: "Devamına Göz At",
};

export const SideCards3 = [
  {
    blogimg: BlogImg,
    section3text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section3text2: "     Kategori: Türkiye'de Sağlık",
    section3text3:
      "     Türkiye’de Covid-19 ile mücadele kapsamında 1 yılı aşkın süredir hem Türk vatandaşları hem de yasal olarak Türkiye’debulunan yabancılar aşı olma hakkında sahipti. Bu kişiler, en yakın sağlık kuruluşlarından randevu alarak aşılarını kolayca ve ücretsiz olarak olabiliyordu... Bu Kişiler, en yakın sağlık kuruluşlarından randevu alarak...",
    userimg: UserImage,
    section3text4: "     Kategori: Türkiye'de Sağlık",
    section3date: "       26 Ocak 2022",
    arrowimg: ArrowImg,
  },

  {
    blogimg: BlogImg,
    section3text1:
      " Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
    section3text2: "     Kategori: Türkiye'de Sağlık",
    section3text3:
      "     Türkiye’de Covid-19 ile mücadele kapsamında 1 yılı aşkın süredir hem Türk vatandaşları hem de yasal olarak Türkiye’debulunan yabancılar aşı olma hakkında sahipti. Bu kişiler, en yakın sağlık kuruluşlarından randevu alarak aşılarını kolayca ve ücretsiz olarak olabiliyordu... Bu Kişiler, en yakın sağlık kuruluşlarından randevu alarak...",
    userimg: UserImage,
    section3text4: "     Kategori: Türkiye'de Sağlık",
    section3date: "       26 Ocak 2022",
    arrowimg: ArrowImg,
  },
];

export const SideCards3data = {
  searchbartext:
    " Türkiye’de covid-19, çalışma izni nasıl alınır?, evlilik yoluyla vatandaşlık başvurusu, Türkiye’de geri gönderme merkezi, yatırım yoluyla Türk vatandaşlığı",
};

export const SideCards4 = {
  section4text1:
    "  Türkiye’de Yabancılar için Pasaport ile Covid-19 Aşısı Uygulaması Başladı!",
  section4text2: "Kategori: Türkiye'de Sağlık",
  section4text3:
    "  Türkiye’de Covid-19 ile mücadele kapsamında 1 yılı aşkın süredir hem Türk vatandaşları hem de yasal olarak Türkiye’de bulunan yabancılar aşı olma hakkında sahipti. Bu kişiler, en yakın sağlık kuruluşlarından randevu alarak aşılarını kolayca ve ücretsiz olarak olabiliyordu... Bu Kişiler, en yakın sağlık kuruluşlarından randevu alarak...",
  userimage: UserImage,
  section4text4: "     Kategori: Türkiye'de Sağlık",
  section4date: "       26 Ocak 2022",
  arrowimg: ArrowImg,
};
