import React from "react";
import { Grid, Typography, Container, Box } from "@mui/material";
import HomeSection2 from "../../components/homeSection2";
import HomeSection1 from "../../components/homeSection1/index";
import HomeSection3 from "../../components/homeSection3/index";
import HomeSection4 from "../../components/homeSection4/index";
import { Pagination } from "@mui/material";
export default function index() {
  const sideCardBlog = [1, 2, 3, 4, 5, 6];
  return (
    <>
      <Container style={{ maxWidth: "1350px" }}>
        <Box>
          <HomeSection1 />
        </Box>
        <Box>
          <HomeSection2 />
        </Box>
        <Box>
          <HomeSection3 />
        </Box>
        <Grid container>
          {sideCardBlog.map(() => (
            <Grid
              item
              lg={4}
              md={6}
              sm={12}
              xs={12}
              style={{ justifyContent: "center", display: "flex" }}
            >
              <HomeSection4 />
            </Grid>
          ))}
        </Grid>
        <div className="border-bottom-main">
          <Box className="border-bottom"></Box>
        </div>
        <div className="pagination">
          <Pagination count={5} shape="rounded" />
        </div>
      </Container>
    </>
  );
}
