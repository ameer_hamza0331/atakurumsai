import React from "react";
import { Grid, Typography } from "@mui/material";
import BlogImage from "../../assets/blogimg.png";
import UserImage from "../../assets/user.png";
import ArrowImage from "../../assets/arrowimg.svg";
import { SideCards4 } from "../../data/data";
import "./index.css";
export default function section4() {
  return (
    <>
      <Grid className="home-section-4-main-card">
        <img src={BlogImage} className="home-section-4-blog-img" />
        <Grid className="home-section-4-card-text">
          <Typography className="home-section-4-text-1">
            {SideCards4.section4text1}
          </Typography>
          <Grid>
            <Typography className="home-section-4-text-2">
              {SideCards4.section4text2}
            </Typography>
          </Grid>
          <Grid>
            <Typography className="home-section-4-text-3">
              {SideCards4.section4text3}
            </Typography>
          </Grid>
          <Grid className="home-section-4-last-section">
            <Grid style={{ display: "flex", alignItems: "center" }}>
              <img src={SideCards4.userimage} />
              <Typography className="home-section-4-last-section-text-1">
                {SideCards4.section4text4}
                <Typography className="home-section-4-last-section-text-2">
                  {SideCards4.section4date}
                </Typography>
              </Typography>
            </Grid>
            <Grid>
              <img src={SideCards4.arrowimg} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
