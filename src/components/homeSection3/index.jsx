import React from "react";
import { Grid, Typography } from "@mui/material";
import BlogImage from "../../assets/blogimg.png";
import UserImage from "../../assets/user.png";
import ArrowImage from "../../assets/arrowimg.svg";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import "./index.css";
import { SideCards3, SideCards3data } from "../../data/data";
import HomeSection4 from "../homeSection4";
export default function section3() {
  // const sideCards2 = [1, 2];

  return (
    <>
      <Grid container mt={6}>
        <Grid item container lg={8} md={12} sm={12} xs={12}>
          {SideCards3.map((props) => (
            <div className="home-section-3-main-div">
              <Grid item lg={6}>
                <Box>
                  <Box>
                    <img src={props.blogimg} className="blog-image-section-3" />
                  </Box>
                </Box>
              </Grid>
              <Grid item lg={6} style={{ padding: "10px" }}>
                <Typography className="section-3-card-text-1">
                  {props.section3text1}
                </Typography>
                <Typography className="section-3-card-text-2">
                  {props.section3text2}
                </Typography>
                <Typography className="section-3-card-text-3">
                  {props.section3text3}
                </Typography>

                <Grid className="home-section-3-last-section">
                  <Grid style={{ display: "flex", alignItems: "center" }}>
                    <img src={props.userimg} />
                    <Typography className="home-section-3-last-section-text-1">
                      {props.section3text4}
                      <Typography className="home-section-3-last-section-text-2">
                        {props.section3date}
                      </Typography>
                    </Typography>
                  </Grid>
                  <Grid>
                    <img src={props.arrowimg} />
                  </Grid>
                </Grid>
              </Grid>
            </div>
          ))}
        </Grid>
        <Grid item lg={4} md={12} sm={12} xs={12} style={{ padding: "10px" }}>
          <Grid className="home-section-3-card-2-div">
            <Box
              sx={{
                width: "auto",
                maxWidth: "100%",
              }}
            >
              <TextField
                className="home-section-3-serach"
                fullWidth
                label="Search"
                id="fullWidth"
              />
            </Box>
            <Grid>
              <Typography className="home-section-3-search-text">
                {SideCards3data.searchbartext}
              </Typography>
            </Grid>
          </Grid>
          <div className="home-sec-4-com" style={{ marginTop: "30px" }}>
            <HomeSection4 />
          </div>
        </Grid>
      </Grid>
    </>
  );
}
