import React from "react";
import { Grid, Typography, Button } from "@mui/material";
import UserImage from "../../assets/user.png";

import { SideCards, SideCards2 } from "../../data/data";
import "./index.css";
export default function section2(props) {
  return (
    <>
      <Grid container mt={3} style={{ justifyContent: "space-between" }}>
        <Grid
          item
          lg={8}
          md={12}
          sm={12}
          xs={12}
          className="home-section-2-slider-main"
        >
          <Grid className="section2-slider-card">
            <Typography className="home-section-2-slider-text">
              {SideCards2.bannertext}
            </Typography>
          </Grid>
        </Grid>
        <Grid
          item
          lg={4}
          md={12}
          sm={12}
          xs={12}
          className="home-section-2-slider-main-card"
        >
          {SideCards.map((props) => (
            <Grid className="home-section-2-cards">
              <Grid className="home-section-2-card-img-main">
                <img
                  src={props.userimage}
                  className="home-section-2-card-img"
                />
              </Grid>
              <Typography className="home-section-2-card-text">
                {props.section2text1}
              </Typography>
              <Grid className="home-section-2-card-info"></Grid>
              <Typography className="home-section-2-card-text-2">
                {props.section2text2}
              </Typography>
            </Grid>
          ))}

          <Grid>
            <Button className="home-section-2-card-btn">
              {SideCards2.buttontext}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
